<?lasso
/**!
    Credit card validation routine that uses the Luhn Algorithm
    to verify CC numbers.

    Valid CC numbers are 13 to 19 digits long.

    @author Jordon Davidson <jordon@jordondavidson.com>
    @url https://gitlab.com/jordonedavidson/cc-number-is-valid

    @param string cc The credit card number to check.
    
    @return boolean True if the provided CC number is valid, false otherwise.
*/
define ccNumberIsValid(cc::string) => {
    // Clean off whitespace.
    #cc->trim

    // Sanity test.
    if (13 > #cc->size or 19 < #cc->size) => {
        // Invalid cc number length.
        return false
    }

    // The Check (final) digit is used in the validation step
    // and is not part of the calculation.
    local(
        'digit' = '',
        'sum' = 0,
        'check_digit' = #cc->get(#cc->size)
    )

    // Sanity check on type of character
    if (not #check_digit->isDigit) => {
        return false
    }

    // Starting from the second digit on the right, loop over the cc digits.
    // Multiply every other one (starting from the first) by 2.
    // If the resulting number is more than 9, subtract 9 from it.
    // Add the resulting number to the sum.
    loop(-from = #cc->length -1, -to = 1, -by = -1) => {
        #digit = #cc->get(loop_count)
        log_always('digit before: ' + #digit)
        // Sanity check on type of character
        if (not #digit->isDigit) => {
            return false
        }
        #digit = (loop_count % 2 == 1 ? integer(#digit) *2 | integer(#digit))
        #digit = (#digit > 9 ? (#digit - 9) | #digit)
        log_always('digit after: ' + #digit->asString)
        #sum += #digit
    }

    // The number is valid if the running sum + the last number mod 10 is 0
    return (#sum + integer(#check_digit)) % 10 == 0
}

?>